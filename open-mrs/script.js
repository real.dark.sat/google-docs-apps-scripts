function getAuthors() {
  // Easiest way to get user ids is to go to
  // https://gitlab.com/api/v4/users?username=USERNAME in a browser

  return [{"name": "tmaczukin", "id": 215818, "manager": "erushton"},
          {"name": "matteeyah", "id": 722076, "manager": "erushton"},
          {"name": "erushton", "id": 2469083, "manager": "erushton"},
          {"name": "iamricecake", "id": 1758950, "manager": "erushton"},
          {"name": "steveazz", "id": 210928, "manager": "erushton"},
          {"name": "drewcimino", "id": 730684, "manager": "erushton"},
          {"name": "fabiopitino", "id": 695467, "manager": "erushton"},
          {"name": "mbobin", "id": 731181, "manager": "erushton"},
          {"name": "shampton", "id": 3029776, "manager": "jhampton"},
          {"name": "jhampton", "id": 3031775, "manager": "jhampton"},
          {"name": "mfluharty", "id": 3374280, "manager": "jhampton"},
          {"name": "filipa", "id": 482476, "manager": "jhampton"},
         ]
}

function projectApiRequest(path) {
  var joiner = '?';
  if (path.indexOf("?")>-1) {
    joiner = '&';
  }
  var url = 'https://gitlab.com/api/v4/' + path
  Logger.log("Making request to: " + url )
  url = url + joiner ;
  var response = UrlFetchApp.fetch(url);
  var json = response.getContentText();
  return JSON.parse(json)
}


function getMRsByAuthor(author) {
  var mrs = [];
  for (var page = 1; page < 10 ; page++) {

    var url = 'groups/9970/merge_requests?state=opened&author_id=' + author + '&per_page=100&page=' + page ;
    mrPage = projectApiRequest(url);
     Logger.log("Found " + mrPage.length + " MRs on this request")
    mrs = mrs.concat(mrPage);
    if ( mrPage.length < 100 ) {
      break;
    }
     Logger.log("Currently tracking " + mrs.length + " MRs total")

  }
  return mrs
}



function getMrAge(mr) {
  var now = new Date()
  mrCreatedAt = new Date(mr.created_at)
  ageInDays = Math.round(Math.abs((now.getTime() - mrCreatedAt.getTime()) / (24*60*60*1000)))
  return ageInDays
}

function getMrStaleness(mr) {
  var now = new Date()
  mrUpdatedAt = new Date(mr.updated_at)
  stalenessInDays = Math.round(Math.abs((now.getTime() - mrUpdatedAt.getTime()) / (24*60*60*1000)))
  return stalenessInDays
}

function isFeature(mr) {
  for (var i = 0 ; i < mr.labels.length; i++) {
    if( mr.labels[i].match(/feature/) ){
      return "TRUE"
    }
  }
  return "FALSE"
}

function isBug(mr) {
  for (var i = 0 ; i < mr.labels.length; i++) {
    if( mr.labels[i].match(/bug/) ){
      return "TRUE"
    }
  }
  return "FALSE"
}

function milestone(mr) {
  try {
    return mr.milestone.title
  } catch (err) {
    return ""
    }
}

function dumpMRsToSpreadsheet(mrs) {
  for (var i = 0; i < mrs.length; i++) {
    // https://gitlab.com/api/v4/groups/9970/merge_requests?state=opened&author_id=215818
    mr = mrs[i]
    writeDataToSheet(mr.author.username, mr.web_url, mr.title, getMrAge(mr), getMrStaleness(mr), mr.work_in_progress, milestone(mr), isFeature(mr), isBug(mr))
  }
}

function writeDataToSheet(username, url, title, age, last_update, wip, milestone, isFeature, isBug) {

  var sheet = SpreadsheetApp.getActiveSheet();
  sheet.appendRow([username, url, title, age, last_update, wip, milestone, isFeature, isBug]);
}

function writeHeaders() {
  var sheet = SpreadsheetApp.getActiveSheet();
  sheet.getRange(1,1,500,12).clearContent();
  sheet.appendRow(["Username", "URL", "Title", "MR Age", "Days since update", "WIP", "Milestone", "IsFeature", "IsBug"]);
}


function myFunction() {
  writeHeaders();
  authors = getAuthors();
  for (var i = 0; i < authors.length; i++) {
      mrs = getMRsByAuthor(authors[i].id)
      dumpMRsToSpreadsheet(mrs)
  }
}
